import { existsSync } from 'node:fs'
import { Low } from 'lowdb'
import { JSONFile } from 'lowdb/node'

export let jsondb_name = './data/plante'
let db_name = './data/plante.json'

/**
 * @returns plante db object using Low if name exists or create is true, otherwise null
 */
export function getDB(name = db_name, create = false) {
    if (create || existsSync(name)) {
        const adapter = new JSONFile<{ plante: any[] }>(name)
        const db = new Low(adapter, { plante: [] })

        return db
    }
    else {
        return null
    }
}

/**
 * @returns -1 if not found, index otherwise
 */
function findLastIndexOfId(d: any[], id: any) {
    let foundI = d.findLastIndex(e =>
        e.id == id
    )

    return foundI
}

/**
 * @returns -1 if there was a problem, otherwise the next free index
 */
function getNextFreeId(d: any[]) {
    const freeId = d.reduce((max, e) => Math.max(max, e.id), -1)
        // max: previousValue (start with initialValue = -1), e: currentValue
        // same as: d.forEach(e => { freeId = Math.max(freeId, e.id) })

    return freeId + 1
}

export async function getData(id: any, db = getDB()) {
    let d = null

    if (db == null) {
        return false
    }
    if (db.data != null) {
        await db.read()
        const { plante } = db.data

        console.log('plante (getData):', plante)

        let i = findLastIndexOfId(plante, id)
        if (i >= 0) {
            d = plante[i]
        }
    }

    return d
}

export async function createData(d: any, db = getDB()) {
    if (db == null) {
        return false
    }
    if (db.data != null) {
        await db.read()
        const { plante } = db.data
        let id = getNextFreeId(plante)
        if (id >= 0) {
            d.id = id
            plante.push(d)
        }
        else {
            // problem while getting a new id in plante db
            return false
        }
    }

    return true
}

export async function writeData(d: any, db = getDB()) {
    if (db == null) {
        return false
    }
    if (db.data != null) {
        await db.read()
        const { plante } = db.data
        let i = findLastIndexOfId(plante, d.id)
        if (i >= 0) {
            plante[i] = d
            await db.write()
        }
        else {
            // id of given object d not found in plante db
            return false
        }
    }

    return true
}

export async function writeTestData(db = getDB()) {
    if (db == null) {
        return false
    }
    if (db.data != null) {
        await db.read()
        const { plante } = db.data  // destructure
        //db.data.plante.push({tag: 'hello world'})
        let freeId = getNextFreeId(plante)
        console.log('freeId:', freeId)
        plante.push({
            id: freeId,
            name: 'Spiderplant' + freeId,
            location: 'Room'
        })
        await db.write()

        console.log('plante: ', plante)

        let findId = 4
        let i = findLastIndexOfId(plante, findId)
        plante[i] = {
            id: findId,
            name: 'Neue4',
            location: 'Bad',
            last_watered: '2022-08-13T12:17:00.000Z'
        }
        console.log('plante with index ', i, ':', plante)
        await db.write()

        await db.read()
        console.log('plante2: ', plante)
    }

    return true
}
