// place files you want to import through the `$lib` alias in this folder.
import { Bezier } from "bezier-js"

/**
 * this function gives a very rough estimate of the available light as a factor of the maximum light
 * - the maximum light factor is always 1 on day 183 of a year
 * - the lowest value is based on the latitude of your location
 * 
 * @param [today=new Date()] current or given date
 * @param [lat=52] latitude of your location (default Berlin)
 */
function availableLightFactor(today: Date = new Date(), lat = 52) {
    let factor = 1
    let minFactor = (90 - lat) / 90
    console.log('min light factor:', minFactor)
    let b = Bezier.quadraticFromPoints({x: 0, y: minFactor}, {x: 0.5, y: 1}, {x: 1, y: minFactor})
    let bYear = b.getLUT(366)
    //console.log(b.getLUT(366))

    console.log('month:', today.getMonth(), 'day:', today.getDate())
    let dayOfYear = today.getMonth() * 30 + today.getDate()
    console.log('day of year:', dayOfYear)
    factor = bYear[dayOfYear].y
    console.log('light factor:', factor)

    return factor
}

export function calculateWatering(data: any) {
    let today = new Date()
    console.log('today:', today)

    let dryoutNeedDays = data.dryout_need_days
    let wateringNeedDays = data.watering_need_days
    console.log(wateringNeedDays)
    let lightFactor = data.light_need_Wm2 / data.max_available_light_Wm2
    wateringNeedDays = wateringNeedDays * lightFactor * (1 / availableLightFactor(today))
        // apply available light vs. needed light only to the base watering need of the plant

    let lastWatered = new Date(data.last_watered)
    console.log('last watered:', lastWatered)

    let originalNextWateringDays = wateringNeedDays + data.skip_watering_offset_days + dryoutNeedDays
    console.log('originalNextWateringDays:', originalNextWateringDays)
    let nextWatering = new Date(lastWatered.getTime() + (originalNextWateringDays * 24 * 60 * 60000))
    console.log('next watering:', nextWatering)

    let nextWateringDays = Math.round((nextWatering.getTime() - today.getTime()) / (60000 * 60 * 24))

    data.next_watering = nextWatering
    data.next_watering_days = nextWateringDays

    return data
}

export function skipWatering(data: any) {
    if (data == null) {
        return {}
    }

    console.log('next_watering_days:', data.next_watering_days)

    if (data.next_watering_days <= 0) {
        // make sure watering can only be skipped when watering is due
        data.skip_watering_offset_days = data.skip_watering_offset_days + 1
    }
    else {
        console.log('no skip, because watering is not due')
    }

    return data
}

export function waterPlant(data: any) {
    const today: Date = new Date()
    if (data == null) {
        return {}
    }

    data.last_watered = today
    if (data.skip_watering_offset_days >= 2) {
        // when watering, reduce watering offset by half
        data.skip_watering_offset_days = data.skip_watering_offset_days / 2
    }
    else {
        data.skip_watering_offset_days = 0
    }

    return data
}