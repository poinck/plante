import { calculateWatering, skipWatering, waterPlant } from '$lib'
import { getDB, getData, writeData } from '$lib/db_plante'
import { json } from '@sveltejs/kit'

export async function GET({ url }) {
    console.log(url.searchParams)
    const paramId: string | null = url.searchParams.get('id')

    let status = 200
    let data = null
    if (paramId != null) {
        data = await getData(paramId)
        console.log(data)

        // calculate watering for requested plant by id
        data = calculateWatering(data)

        const paramWater: string | null = url.searchParams.get('water')
        if (paramWater != null) {
            let saved = true
            if (paramWater == 'done') {
                data = waterPlant(data)
                data = calculateWatering(data)
                    // recalculate after watering
                saved = await writeData(data)
            }
            else if (paramWater == 'skip') {
                data = skipWatering(data)
                data = calculateWatering(data)
                    // recalculate after skipping

                console.log('skip watering:', data)

                saved = await writeData(data)

                console.log('saved after skip watering:', data)
            }
            if (! saved) {
                status = 500
                data = {id: parseInt(paramId), message: 'Plant not saved'}
            }
        }

        if (data == null) {
            status = 404
            data = {id: parseInt(paramId), message: 'Plant not found'}
        }
        else {
            console.log ('data (only paramId given)', data)
            if (Object.keys(data).length == 0) {
                status = 404
                data = {id: parseInt(paramId), message: 'Plant not found'}
            }
        }

        // calculate watering for requested plant by id
        //data = calculateWatering(data)
    }
    else {
        data = {id: 0}
            // TODO  think of what could be done here in the future
    }

    console.log('GET complete:', data)

    return json(data, {status: status})
}
