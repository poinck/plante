import { getDB, writeTestData } from '$lib/db_plante'
import { json } from '@sveltejs/kit'

export async function GET({ url }) {
    console.log(url.searchParams)
    const paramTest: string | null = url.searchParams.get('test')

    let data = null
    if (paramTest != null) {
        if (paramTest == 'write') {
            await writeTestData()
        }
    }

    return json(data, {status: 200})
}
