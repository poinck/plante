# Readme: plante

Plante aims to be a basic plant helping agent reminding you to water your plants based on your knowledge of them. Fertilize and repotting reminders are also planned in the furture as well as taking the current weather forecast into account.

## Sample

```.ts
{
  id: 1,
  name: 'Spiderplant',
  light_need_Wm2: 500,
  watering_need_days: 7,
  dryout_need_days: 0,
  max_available_light_Wm2: 300,
  pot_size_cm: 10,
  location: 'Room',
  skip_watering_offset_days: 2,
  last_watered: 2023-09-06T11:54:00.000Z,
  next_watering: 2023-09-19T11:54:00.000Z,
  next_watering_days: 9
}
```

- `pot_size_cm` is for future estimations for repotting suggestions. May also need more plant parameters to be acurate.